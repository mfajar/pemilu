<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePemilihsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
A     */
     public function up()
    {
        Schema::create('pemilihs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('nik')->unique();
            $table->string('nama');
            $table->string('jenis_kelamin');
            $table->string('golongan_darah');
            $table->string('alamat');
            $table->string('agama');
            $table->string('status');
            $table->string('pekerjaan');
            $table->string('kewarganegaraan');
            $table->string('masa_berlaku');
            $table->string('foto')->unique();
            $table->string('tanggal_pembuatan');
            $table->string('tempat_pembuatan');
            $table->bigInteger('id_rt')->unsigned();
             // $table->foreign('id_rt') // foreignKey 
             //  ->references('id') // dari kolom id 
             //  ->on('rts') // di tabel users
             //  ->onUpdate('cascade') // ketika terjadi perubahan di tabel users maka akan update
             //  ->onDelete('cascade'); 
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pemilihs');
    }
}
