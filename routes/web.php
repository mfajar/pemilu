<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',function(){
 return	redirect('/home');
});

Route::get('/home', 'UtamaController@home');
Route::get('/profile', 'UtamaController@profile');
Route::get('/profile/calon/{id}', 'UtamaController@show');
Route::get('/rekapitulasi', 'UtamaController@rekap');

Route::get('/home/login', 'LoginPemilihController@login');

Route::post('/home/logout', 'UtamaController@store');
Route::post('/loginPost', 'LoginPemilihController@loginPost');
Route::get('/register', 'User@register');
Route::post('/registerPost', 'User@registerPost');
Route::get('/logout', 'User@logout');


Route::get('/admin', 'AdminController@index');
Route::put('/admin/lokasi', 'PagecalonController@updatelokasi');


Route::get('/admin/kelurahan', 'PageKelurahanController@index');
Route::get('/admin/kelurahan/tambah', 'PageKelurahanController@tambah');
Route::post('/admin/kelurahan/tambah', 'PageKelurahanController@store');
Route::get('/admin/kelurahan/edit/{id}', 'PageKelurahanController@edit');
Route::put('/admin/kelurahan/edit/{id}', 'PageKelurahanController@update');
Route::delete('/admin/kelurahan/delete/{id}', 'PageKelurahanController@destroy');

Route::get('/admin/rw', 'PageRwController@index');
Route::get('/admin/rw/create', 'PageRwController@create');
Route::post('/admin/rw/create', 'PageRwController@store');
Route::get('/admin/rw/edit/{id}', 'PageRwController@edit');
Route::put('/admin/rw/edit/{id}', 'PageRwController@update');
Route::delete('/admin/rw/delete/{id}', 'PageRwController@destroy');


Route::get('/admin/rt', 'PagertController@index');
Route::get('/admin/rt/create', 'PagertController@create');
Route::post('/admin/rt/create', 'PagertController@store');
Route::get('/admin/rt/edit/{id}', 'PagertController@edit');
Route::put('/admin/rt/edit/{id}', 'PagertController@update');
Route::delete('/admin/rt/delete/{id}', 'PagertController@destroy');

Route::get('/admin/pemilih', 'PagepemilihController@index');
Route::get('/admin/pemilih/create', 'PagepemilihController@create');
Route::post('/admin/pemilih/create', 'PagepemilihController@store');
Route::get('/admin/pemilih/edit/{id}', 'PagepemilihController@edit');
Route::get('/admin/pemilih/view/{id}', 'PagepemilihController@show');
Route::put('/admin/pemilih/edit/{id}', 'PagepemilihController@update');
Route::delete('/admin/pemilih/delete/{id}', 'PagepemilihController@destroy');

Route::get('/admin/calon', 'PagecalonController@index');
Route::get('/admin/calon/create', 'PagecalonController@create');
Route::post('/admin/calon/create', 'PagecalonController@store');
Route::get('/admin/calon/edit/{id}', 'PagecalonController@edit');
Route::get('/admin/calon/view/{id}', 'PagecalonController@show');
Route::put('/admin/calon/edit/{id}', 'PagecalonController@update');
Route::delete('/admin/calon/delete/{id}', 'PagecalonController@destroy');


Auth::routes();

// // Route::get('/home', 'HomeController@index')->name('home');

// // Route::get('/', function(){
// //     return view('utama.home');
// // })->name('homepage');
// Route::get('home-login','Auth\HomeLoginController@showLoginForm');
// Route::post('home-login', ['as' => 'pemilih-login', 'uses' => 'Auth\HomeLoginController@login']);
// Route::get('pemilih-register','Auth\HomeLoginController@showRegisterPage');
// Route::post('pemilih-register', 'Auth\HomeLoginController@register')->name('admin.register');