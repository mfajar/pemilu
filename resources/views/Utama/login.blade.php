@extends('layouts.master')

@section('title','Login')

@section('nav')
<a class="nav-item nav-link active" href="/" style="">Home <span class="sr-only">(current)</span></a>
      <a class="nav-item nav-link" href="/profile">Profile</a>
      <a class="nav-item nav-link" href="/rekapitulasi">Rekapitulasi</a>
@endsection

@section('main')

    <!-- Main Section -->
    <section class="main-jari">
        <!-- Add Your Content Inside -->
        <div class="content">
            <!-- Remove This Before You Start -->
            
           
            @if(\Session::has('alert-success'))
                <div class="alert alert-success">
                    <div>{{Session::get('alert-success')}}</div>
                </div>
            @endif
            <form action="{{ url('/loginPost') }}" method="post" style="text-align: center;">
                {{ csrf_field() }}
               <img src="{{url('utama/finger.png')}}" width="300" height="300" style="margin-top: 50px" alt="">
               <br><br>
               <h2>Masukan Sidik</h2>
               <h2>Jari Anda</h2>
            </form>
        </div>
        <!-- /.content -->
    </section>
    <!-- /.main-section -->
@endsection


@section('script')
 
@endsection
