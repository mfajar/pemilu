@extends('layouts.admin')


@section('title','Calom')

@section('content')
		 <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">Calon</li>
        </ol>

        <!-- DataTables Example -->
        <div class="card mb-3">
          <form action="/admin/calon/create" method="post" enctype="multipart/form-data">
          <div class="card-header">
            <label for="nama">Nama : </label>
            <input type="text" id="nama"  name="nama" class="form-control" placeholder="Nama Lengkap" required="">	
            <label for="">Jenis Kelamin : </label>
            <input type="radio"  value="Laki-laki" class="form" id="laki"  name="gender"><label for="laki">Laki-Laki</label>
            <input required="" id="pr" type="radio"  value="Perempuan" class="form" name="gender"><label for="pr">Perempuan</label><br>
             <label for="agama">Agama : </label>
             <select required="" class="form-control" name="agama" id="agama">
              <option value="islam">Islam</option>
              <option value="Kristen Protestan">Kristen Protestan</option>
              <option value=" Katolik"> Katolik</option>
              <option value="Hindu">Hindu</option>
              <option value="Buddha">Buddha</option>
              <option value=" Kong Hu Cu"> Kong Hu Cu</option>
            </select>           <br>
                        <label for="status">Staus : </label>
            <input type="radio" value="menikah" name="status"> Menikah
            <input checked="" type="radio" value="belum menikah" name="status"> Belum Menikah <br>
           
            <label  for="alamat">Tempat,tangal lahir   :</label> <br>
            <input type="text" class="form-control" id="alamat" name="ttl" required="" placeholder="Tmpat, tanggal bulan tahun">
            <label for="kerja">Pekerjaan : </label>
            <input type="text" id="kerja" required=""  name="pekerjaan" class="form-control" placeholder="Pekerjaan">
            <label for="umur">Umur : </label>
            <input type="number" id="umur" required=""  name="umur" class="form-control" placeholder="Umur">
            <label for="calon">Calon : </label>
            <input type="radio" value="rt" name="calon" id="rt"> <label for="rt">RT</label>
            <input checked="" type="radio" id="rw" value="rw" name="calon"> <label for="rw">RW</label> <br>
            <label for="visi">Visi : </label>
            <textarea name="visi" id="visi" cols="30" class="form-control" rows="2"></textarea>
            <label for="misi">Misi : </label>
            <textarea name="misi" id="misi" cols="30" class="form-control" rows="5"></textarea>
            <div class="form-group">
                                <label for="">Pilih gambar</label>
                                <input type="file" name="foto">
                                <p class="text-danger">{{ $errors->first('image') }}</p>
            </div>

				



          
          </div>
          {{csrf_field()}}
          <div class="card-footer"><button class="btn btn-primary" type="submit"><i class="fas fa-plus"></i>
            Tambah Calon</button>
          </div>
          </form>
        </div>




@endsection

@section('sidebar')
 <ul class="sidebar navbar-nav">
      <li class="nav-item ">
        <a class="nav-link" href="/admin">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span>
        </a>w
      </li>
      <li class="nav-item ">
        <a class="nav-link " href="/admin/pemilih">
           <i class="fas fa-fw fa-folder"></i>
          <span>Pemilih</span>
        </a>
      </li>
      <li class="nav-item active">
        <a class="nav-link " href="/admin/calon">
           <i class="fas fa-fw fa-folder"></i>
          <span>Calon</span>
        </a>
      </li>
      
      <li class="nav-item ">
        <a class="nav-link " href="/admin/kelurahan">
           <i class="fas fa-fw fa-folder"></i>
          <span>Kelurahan</span>
        </a>
      </li>
      <li class="nav-item ">
        <a class="nav-link" href="/admin/rw">
          <i class="fas fa-fw fa-folder"></i>
          <span>RW</span>
        </a>
      </li>
      <li class="nav-item ">
        <a class="nav-link" href="/admin/rt">
          <i class="fas fa-fw fa-folder"></i>
          <span>RT</span>
        </a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-fw fa-folder"></i>
          <span>Pages</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
          <h6 class="dropdown-header">Login Screens:</h6>
          <a class="dropdown-item" href="login.html">Login</a>
          <a class="dropdown-item" href="register.html">Register</a>
          <a class="dropdown-item" href="forgot-password.html">Forgot Password</a>
          <div class="dropdown-divider"></div>
          <h6 class="dropdown-header">Other Pages:</h6>
          <a class="dropdown-item" href="404.html">404 Page</a>
          <a class="dropdown-item" href="blank.html">Blank Page</a>
        </div>
      </li>
    </ul>
@endsection
