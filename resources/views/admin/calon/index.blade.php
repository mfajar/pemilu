@extends('layouts.admin')


@section('title','Calon')

@section('content')
   

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">Calon</li>
        </ol>

        <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
          <a href="/admin/calon/create"><button class="btn btn-primary"><i class="fas fa-plus"></i>
            Tambah Calon</button></a>

          <form action="/admin/lokasi" style="display: inline;" class="ml-5" method="post">
           <label for="rt">RT </label> <input type="number" value="{{$tempat->rt}}" id="rt" name="rt">

           <label for="rw">RW </label> <input type="number" value="{{$tempat->rw}}" id="rw" name="rw">
              {{csrf_field()}}
     {{method_field('PUT')}}
          <button type="submit" class="btn btn-primary"><i class="fas fa-update"></i>
            Update RT/RW</button>

          </form>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <td>No</td>
                    
                    <th>Nama</th>

                    <th>Jenis Kelamin</th>
                    <th>Agama</th>
                    <th>Status</th>
                    <th>TTL</th>
                    <th>Pekerjaan</th>
                    <th >Action</th>
                    
                  </tr>
                </thead>
                
                <tbody>

                @foreach($tabel as $item)
                  <tr>  
                    <td>{{$loop -> iteration}}</td>
                    <td>{{$item->nama}}</td>
                    <td>{{$item->gender}}</td>
                     <td>{{$item->agama}}</td>
                      <td>{{$item->status}}</td>
                      <td>{{$item->ttl}}</td>
                      <td>{{$item->pekerjaan}}</td>
                    <td>
                      <a href="/admin/calon/view/{{$item->id}}"><button class="btn btn-info">Lihat</button></a>
                      <a href="/admin/calon/edit/{{$item->id}}"><button class="btn btn-warning">Ubah</button></a>
                      <form action="/admin/calon/delete/{{$item->id}}"  style="display: inline;" method="post">
                        {{csrf_field()}}
                        {{method_field('DELETE')}}
        
                         <input type="submit" class="btn btn-danger" value="Delete"  onclick="return confirm('Anda yakin mau menghapus item ini ?')">

                    </form></td>
                  </tr>
                  @endforeach

                 
                </tbody>
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>
@endsection

@section('sidebar')
 <ul class="sidebar navbar-nav">
      <li class="nav-item ">
        <a class="nav-link" href="/admin">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span>
        </a>w
      </li>
      <li class="nav-item ">
        <a class="nav-link " href="/admin/pemilih">
           <i class="fas fa-fw fa-folder"></i>
          <span>Pemilih</span>
        </a>
      </li>
       <li class="nav-item active">
        <a class="nav-link " href="/admin/calon">
           <i class="fas fa-fw fa-folder"></i>
          <span>Calon</span>
        </a>
      </li>
      
      <li class="nav-item ">
        <a class="nav-link " href="/admin/kelurahan">
           <i class="fas fa-fw fa-folder"></i>
          <span>Kelurahan</span>
        </a>
      </li>
      <li class="nav-item ">
        <a class="nav-link" href="/admin/rw">
          <i class="fas fa-fw fa-folder"></i>
          <span>RW</span>
        </a>
      </li>
      <li class="nav-item ">
        <a class="nav-link" href="/admin/rt">
          <i class="fas fa-fw fa-folder"></i>
          <span>RT</span>
        </a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-fw fa-folder"></i>
          <span>Pages</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
          <h6 class="dropdown-header">Login Screens:</h6>
          <a class="dropdown-item" href="login.html">Login</a>
          <a class="dropdown-item" href="register.html">Register</a>
          <a class="dropdown-item" href="forgot-password.html">Forgot Password</a>
          <div class="dropdown-divider"></div>
          <h6 class="dropdown-header">Other Pages:</h6>
          <a class="dropdown-item" href="404.html">404 Page</a>
          <a class="dropdown-item" href="blank.html">Blank Page</a>
        </div>
      </li>
    </ul>
@endsection
