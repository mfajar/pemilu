@extends('layouts.admin')


@section('title','Dashboard')

@section('content')

 <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">Suara</li>
        </ol>

        <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
          <h3> Calon RT </h3>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <td>No</td>
                    <th>ID</th>
                    <th>Nama</th>
                    <th>Total Suara </th>
                    <th>Action</th>
                  </tr>
                </thead>
                
                <tbody>
                  @foreach($rt as $item)
                  <tr>  
                    <td>{{$loop -> iteration}}</td>
                    <td>{{$item->id}}</td>
                    <td>{{$item->getCalon->nama}}</td>
                    <td>{{$item->suara}}</td>
                    <td><form action="/admin/suara/delete/{{ $item->id}}"  style="display: inline;" method="post">
                        {{csrf_field()}}
                        {{method_field('DELETE')}}
        
                         <input type="submit" class="btn btn-danger" value="Delete"  onclick="return confirm('Anda yakin mau menghapus item ini ?')">

                    </form></td>
                  </tr>

                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>
         <div class="card mb-3">
          <div class="card-header">
          <h3> Calon RW </h3>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <td>No</td>
                    <th>ID</th>
                    <th>Nama</th>
                    <th>Total Suara </th>
                    <th>Action</th>
                  </tr>
                </thead>
                
                <tbody>
                  @foreach($rw as $item)
                  <tr>  
                    <td>{{$loop -> iteration}}</td>
                    <td>{{$item->id}}</td>
                    <td>{{$item->getCalon->nama}}</td>
                    <td>{{$item->suara}}</td>
                    <td><form action="/admin/suara/delete/{{ $item->id}}"  style="display: inline;" method="post">
                        {{csrf_field()}}
                        {{method_field('DELETE')}}
        
                         <input type="submit" class="btn btn-danger" value="Delete"  onclick="return confirm('Anda yakin mau menghapus item ini ?')">

                    </form></td>
                  </tr>

                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>


@endsection

@section('sidebar')
 <ul class="sidebar navbar-nav">
      <li class="nav-item  active">
        <a class="nav-link" href="/admin">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span>
        </a>
      </li>
      
      <li class="nav-item">
        <a class="nav-link " href="/admin/pemilih">
           <i class="fas fa-fw fa-folder"></i>
          <span>Pemilih</span>
        </a>
      </li>
       <li class="nav-item ">
        <a class="nav-link " href="/admin/calon">
           <i class="fas fa-fw fa-folder"></i>
          <span>Calon</span>
        </a>
      </li>
      
      <li class="nav-item">
        <a class="nav-link " href="/admin/kelurahan">
           <i class="fas fa-fw fa-folder"></i>
          <span>Kelurahan</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/admin/rw">
          <i class="fas fa-fw fa-folder"></i>
          <span>RW</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/admin/rt">
          <i class="fas fa-fw fa-folder"></i>
          <span>RT</span>
        </a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-fw fa-folder"></i>
          <span>Pages</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
          <h6 class="dropdown-header">Login Screens:</h6>
          <a class="dropdown-item" href="login.html">Login</a>
          <a class="dropdown-item" href="register.html">Register</a>
          <a class="dropdown-item" href="forgot-password.html">Forgot Password</a>
          <div class="dropdown-divider"></div>
          <h6 class="dropdown-header">Other Pages:</h6>
          <a class="dropdown-item" href="404.html">404 Page</a>
          <a class="dropdown-item" href="blank.html">Blank Page</a>
        </div>
      </li>
    </ul>
@endsection
