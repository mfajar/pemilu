<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kelurahan extends Model
{
    protected $table = "kelurahans";
    protected $fillable = ['nama'] ;


     public function getManyRw()
    {
        return $this->hasMany('App\rw');
    }
}
