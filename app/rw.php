<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class rw extends Model
{
    protected $table ='rws';
    protected $fillable = ['nama','id_kel'];

     public function getKel()
    {
    	return $this->belongsTo('App\Kelurahan','id_kel');
    }


     public function getManyRt()
    {
        return $this->hasMany('App\rt');
    }
}
