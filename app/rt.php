<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class rt extends Model
{
    protected $table = 'rts';
    protected $fillable = ['nama','id_rw'];


      public function getRw()
    {
    	return $this->belongsTo('App\rw','id_rw');
    }
     public function getManyPemilih()
    {
        return $this->hasMany('App\Pemilih');
    }
}
