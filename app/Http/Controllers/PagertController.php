<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\rt;

class PagertController extends Controller
{       
     public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {		
    	$tabel = rt::all();

    	return view('admin.rt.index',compact("tabel"));
    }
    public function create()
    {
    	return view('admin.rt.create');
    }
    public function store(Request $req)
    {
    	$rt = new rt;
    	$rt->nama = $req->nama;
    	$rt->id_rw = $req->id_rw;
    	$rt->save();
    	return redirect('/admin/rt');

    }
    public function edit($id)
    {
    	$rt = rt::find($id);
    	return view('admin.rt.edit',compact('rt'));
    }
    public function update(Request $req,$id)
    {
    	$rt = rt::find($id);
    	$rt->nama = $req->nama;
    	$rt->id_rw = $req->id_rw;
    	$rt->save();
    	return redirect('/admin/rt');

    }
    public function destroy($id)
    {
    	rt::destroy($id);
    	return redirect('/admin/rt');

    }
}
