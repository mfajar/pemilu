<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kelurahan;
class PageKelurahanController extends Controller
{       
     public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {	$kelurahan = kelurahan::all();
    	return view('admin.kelurahan.index',compact('kelurahan'));
    }
    public function tambah()
    {
    	return view('admin.kelurahan.create');
    }
    public function store(Request $req)
    {
    	$tabel = new Kelurahan;
    	$tabel->nama = $req->nama;
    	$tabel->save();
    	return redirect('admin/kelurahan');
    }
    public function edit($id)
    {
    	$kelurahan = kelurahan::find($id);
    	return view('admin.kelurahan.edit',compact("kelurahan"));
    }
    public function update(Request $req,$id)
    {
    	$kelurahan = kelurahan::find($id);
    	$kelurahan->nama = $req->nama;
    	$kelurahan->save();
    	return redirect('admin/kelurahan');
    }
    public function destroy($id)
    {
        kelurahan::destroy($id);
        return redirect('admin/kelurahan');
    }
}
