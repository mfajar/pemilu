<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Calon;
use App\Suara;
use App\Daerah;
use Carbon\Carbon;
use Image;
use File;

class PagecalonController extends Controller
{

    
    public function index()
    {	$daerah = daerah::all();

      if( sizeof($daerah)<1 ){
        $baru = new Daerah;
        $baru->rt = 01;
        $baru->rw = 01;
        $baru->save();
      }
      else{
        $tempat = daerah::find(1);
      }


      $tabel = calon::all();
    	return view('admin.calon.index',compact('tabel','tempat'));
    }
    public function create()
    {
    	return view('admin.calon.create');
    }
    


    public $path;
    public $dimensions;

    public function __construct()
    {    $this->middleware('auth');
    
        //DEFINISIKAN PATH
        $this->path = public_path('calon');
        //DEFINISIKAN DIMENSI
        $this->dimensions = ['245', '300'];
    }
	
    public function store(Request $req)
    {
    	$calon = new calon;
    	 $this->validate($req, [
            'foto' => 'required|image|mimes:jpg,png,jpeg'
        ]);
        if (!File::isDirectory($this->path)) {
            File::makeDirectory($this->path);
        }
		
        $file = $req->file('foto');
        
        $fileName = Carbon::now()->timestamp . '_' . uniqid() . '.' . $file->getClientOriginalExtension();
        
        Image::make($file)->save($this->path . '/' . $fileName);
		
        
        foreach ($this->dimensions as $row) {
        
            $canvas = Image::canvas($row, $row);                
            $resizeImage  = Image::make($file)->resize($row, $row, function($constraint) {
                $constraint->aspectRatio();
            });			        
            if (!File::isDirectory($this->path . '/' . $row)) {       
                File::makeDirectory($this->path . '/' . $row);
            }        	
            $canvas->insert($resizeImage, 'center');
            $canvas->save($this->path . '/' . $row . '/' . $fileName);
        }
        
       $calon->nama = $req->nama;
       $calon ->gender= $req->gender;
       $calon->agama = $req->agama;
       $calon->pekerjaan = $req->pekerjaan;
       $calon->status = $req->status;
       $calon->foto = $fileName;
       $calon->umur = $req->umur;
       $calon->visi = $req->visi;
       $calon->misi= $req->misi;
       $calon->ttl = $req->ttl;
       $calon->save();

       $calon = calon::orderBy('id','desc')->first();
	   $suara = new Suara;
	   $suara->id_calon = $calon->id;
	   $suara->calon = $req->calon;
	   $suara->suara = 0;
	   $suara->save();
       return redirect('/admin/calon');
    }
    public function updatelokasi(Request $req)
    {
      $tempat = daerah::find(1);
      $tempat->rt = $req->rt;
      $tempat->rw = $req->rw;
      $tempat->save();
      return redirect("/admin/calon");
    }
}
