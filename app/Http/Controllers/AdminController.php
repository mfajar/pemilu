<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Suara;

class AdminController extends Controller
{
	 public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {	$rt = suara::where('calon','rt')->get();
    	$rw = suara::where('calon','rw')->get();
    	return view('admin.index',compact('rt','rw'));
    }
    

}
