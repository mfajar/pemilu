<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\rw;

class PageRwController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {		
    	$tabel = rw::all();

    	return view('admin.rw.index',compact("tabel"));
    }
    public function create()
    {
    	return view('admin.rw.create');
    }
    public function store(Request $req)
    {
    	$rw = new rw;
    	$rw->nama = $req->nama;
    	$rw->id_kel = $req->id_kel;
    	$rw->save();
    	return redirect('/admin/rw');

    }
    public function edit($id)
    {
    	$rw = rw::find($id);
    	return view('admin.rw.edit',compact('rw'));
    }
    public function update(Request $req,$id)
    {
    	$rw = rw::find($id);
    	$rw->nama = $req->nama;
    	$rw->id_kel = $req->id_kel;
    	$rw->save();
    	return redirect('/admin/rw');

    }
     public function destroy($id)
    {
        rw::destroy($id);
        return redirect('/admin/rw');

    }
}
